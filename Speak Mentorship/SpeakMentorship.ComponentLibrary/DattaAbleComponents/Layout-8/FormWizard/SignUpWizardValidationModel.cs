﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SpeakMentorship.ComponentLibrary.DattaAbleComponents.Layout_8.FormWizard
{
    public class SignUpWizardValidationModel
    {

        [Required(ErrorMessage = "first name is required.")]
        [StringLength(10, ErrorMessage = "first name is too long.")]
        public string FirstName { get; set; }

        [Required(ErrorMessage = "last name is required.")]
        [StringLength(10, ErrorMessage = "last name is too long.")]
        public string LastName { get; set; }


        [MaxLength(250, ErrorMessage = "last name is too long.")]
        public string Description { get; set; }

        [Required(ErrorMessage = "age is required.")]
        public string Age { get; set; } = string.Empty; // new DateTime(DateTime.Now.Year, DateTime.Now.Month, 28);

        [Required(ErrorMessage = "select your gender.")]
        public bool Gender { get; set; } = true;

        [Required(ErrorMessage = "select a role.")]
        public bool Role { get; set; } = false;


        [Required(ErrorMessage = "select a interest.")]
        public string[] Interest { get; set; }

        [Required(ErrorMessage = "email is required.")]
        [EmailAddress()]
        public string Email { get; set; }

        [Required(ErrorMessage = "password is required.")]
        [MinLength(8, ErrorMessage = "password must be atleast 8 characters")]
        [DataType(DataType.Password)]
        public string Password { get; set; } = string.Empty;

        [Required(ErrorMessage = "re-type password is required.")]
        [Compare(nameof(Password), ErrorMessage = "password dosent match")]
        public string ConfirmPassword { get; set; } = string.Empty;
    }
}
