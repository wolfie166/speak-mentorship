﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SpeakMentorship.ComponentLibrary.DattaAbleComponents.Layout_8.GridForm
{
    public class LogInFormValidationModel
    {
        [Required(ErrorMessage = "email is required")]
        public string Email { get; set; }

        [Required(ErrorMessage = "password is required")]
        public string Password { get; set; }

        public bool RememberMe { get; set; } = false;
    }
}
