﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakMentorship.ComponentLibrary.DattaAbleComponents.Layout_8.Table.MentorshipTables
{
   public class MentorshipSessionTableItem
    {
        public string RoomId { get; set; } = string.Empty;
        public string ImageUrl { get; set; } = string.Empty;
        public string SessionTitle { get; set; } = string.Empty;
        public string SessionSubTitle { get; set; } = string.Empty;
        public string Date { get; set; } = string.Empty;
        public string Status { get; set; } = string.Empty;
    }
}
