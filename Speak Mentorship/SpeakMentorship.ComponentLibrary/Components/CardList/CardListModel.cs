﻿using SpeakMentorship.ComponentLibrary.Components.Card;
using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakMentorship.ComponentLibrary.Components.CardList
{
    public class CardListModel
    {
       public List<BootstrapCardModel> Cards { get; set; }
    }
}
