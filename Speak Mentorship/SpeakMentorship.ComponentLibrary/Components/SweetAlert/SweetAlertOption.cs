﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakMentorship.ComponentLibrary.Components.SweetAlert
{
    public class SweetAlertOption
    {
        
        public string Title { get; set; }
        public string Text { get; set; }
        public string Type { get; set; }
        public bool ShowCancelButton { get; set; }
        public string ConfirmButtontext { get; set; }
        public string CancelButtonText { get; set; }
    }
}
