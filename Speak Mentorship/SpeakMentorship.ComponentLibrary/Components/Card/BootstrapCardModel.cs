﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakMentorship.ComponentLibrary.Components.Card
{
    public class BootstrapCardModel
    {

        public string MainImage { get; set; } = string.Empty;

        public string Title { get; set; } = string.Empty;

        public string SubTitle { get; set; } = string.Empty;

        public string ProfileImage { get; set; } = string.Empty;

        public string ProfileTitle { get; set; } = string.Empty;


        public string ProfileDescription { get; set; } = string.Empty;


        public CardMode Mode { get; set; } = CardMode.Request;
        public EndUser WhoIsEndUser { get; set; } = EndUser.Mentee;
        public string Status { get; set; } = string.Empty;


    }

    public enum CardMode{
        Selection,
        Request

    }

    public enum EndUser
    {
        Mentee,
            Mentor
    }
}
