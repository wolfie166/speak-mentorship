﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace SpeakMentorship.ComponentLibrary.Components.ResetPassword
{
    public class ResetPasswordValidation
    {
        [Required(ErrorMessage = "email is required")]
        public string Email { get; set; }
    }
}
