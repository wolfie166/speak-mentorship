﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakMentorship.ComponentLibrary.Components.ResetPassword
{
    public enum ResetPasswordViewSelection
    {
        RequestPasswordReset,
        EmailSent,
        PasswordConfirmation,
        PasswordChange
    }
}
