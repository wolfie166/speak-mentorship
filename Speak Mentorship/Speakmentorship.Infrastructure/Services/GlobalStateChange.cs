﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Speakmentorship.Infrastructure.Services
{
    public class GlobalStateChange
    {

        public void InvokeStateChange()
        {
            Console.WriteLine("State Has Changed");
            this.StateHasChanged?.Invoke(this, new EventArgs());
        }

        public event EventHandler StateHasChanged;

    }
}
