﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Speakmentorship.Infrastructure.Services
{
    public interface IGlobalStateHasChange
    {
        void InvokeStateChange();

        event EventHandler StateHasChanged;
    }
}
