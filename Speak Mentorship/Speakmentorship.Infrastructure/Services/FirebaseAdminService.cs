﻿using FirebaseAdmin;
using FirebaseAdmin.Auth;
using Google.Apis.Auth.OAuth2;
using SpeakMentoriship.Core.DTO.SpeakmentorshipDTO;
using SpeakMentoriship.Core.Services;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Speakmentorship.Infrastructure.Services
{
    public class FirebaseAdminService : IFirebaseAdminService
    {
        private FirebaseApp _firebaseApp { get; set; }
        private FirebaseAuth _firebaseAuth { get; set; }
        public FirebaseAdminService()
        {
            _firebaseApp = FirebaseApp.Create(new AppOptions()
            {
                Credential = GoogleCredential.FromFile("speakmentorship-firebase.json"),
            });
        }
        private async Task<bool> CreateUserAsync(BaseUser newUser)
        {
            UserRecordArgs args = new UserRecordArgs()
            {
                Email = newUser.Email,
                EmailVerified = newUser.EmailVerified,
                PhoneNumber = newUser.PhoneNumber,
                Password = newUser.Password,
                DisplayName = newUser.DisplayName,
                PhotoUrl = newUser.PhotoUrl,
                Disabled = newUser.Disabled,
            };

            UserRecord userRecord;
            try {
                userRecord = await FirebaseAuth.DefaultInstance.CreateUserAsync(args);

                // See the UserRecord reference doc for the contents of userRecord.
                Console.WriteLine($"Successfully created new user: {userRecord.Uid}");

               
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                return false;
            }
            return true;

        }
        public async Task<bool> CreateUser(BaseUser newUser)
        {
            return await CreateUserAsync(newUser);
        }

        private async Task<bool> UpdateUserAsync(string userId, BaseUser updatedUser)
        {
            UserRecordArgs args = new UserRecordArgs()
            {
                Uid = userId,
                Email = updatedUser.Email,
                EmailVerified = updatedUser.EmailVerified,
                PhoneNumber = updatedUser.PhoneNumber,
                Password = updatedUser.Password,
                DisplayName = updatedUser.DisplayName,
                PhotoUrl = updatedUser.PhotoUrl,
                Disabled = updatedUser.Disabled,
            };

            UserRecord userRecord;
            try
            {
                userRecord = await FirebaseAuth.DefaultInstance.UpdateUserAsync(args);

                // See the UserRecord reference doc for the contents of userRecord.
                Console.WriteLine($"Successfully created new user: {userRecord.Uid}");


            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                return false;
            }
            return true;
        }
        public async Task<bool> UpdateUser(string userId, BaseUser updatedUser)
        {
            return await UpdateUserAsync(userId, updatedUser);
        }

        public Task<bool> DeleteUser()
        {
            throw new NotImplementedException();
        }

        private async Task<bool> LogInUserAsync()
        {
            throw new NotImplementedException();
        }
        public async Task<bool> LogInUser()
        {
            throw new NotImplementedException();
        }

        public Task<bool> LogOutUser()
        {
            throw new NotImplementedException();
        }

        private async Task<bool> VerifyUserIdToken(string token)
        {
            try
            {
                FirebaseToken decodedToken = await FirebaseAuth.DefaultInstance
           .VerifyIdTokenAsync(token);
                if (decodedToken.Uid != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
                
            }
            catch (Exception e)
            {
                return false;
            }
        }
        public async Task<bool> VerifyIdToken(string token)
        {
            return await VerifyUserIdToken(token);
        }

        private async Task<string> CreateCustomTokenAsync(string uid)
        {
            string customToken = await FirebaseAuth.DefaultInstance.CreateCustomTokenAsync(uid);
            return customToken;
        }
        public async Task<string> CreateCustomToken(string uid)
        {
            return await CreateCustomTokenAsync(uid);
        }
    }
}
