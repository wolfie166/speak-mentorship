﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SpeakMentoriship.Core.DTO.SpeakmentorshipDTO;
using SpeakMentoriship.Core.Services;
using Speakmentorship.Infrastructure.JsInterop;

namespace Speakmentorship.Infrastructure.Services
{
    public class FirebaseJsService : IFirebaseJsService
    {
        private BaseUser _user { get; set; }
        private IJSRuntime _jsRuntime;
        private IUriHelper _uriHelper;
        private FirebaseInteropService _firebaseInterop { get; set; }
        private FirebaseAdminService _firebaseAdminService { get; set; }
        private GlobalStateChange _globalStateHasChanged { get; set; }
        public FirebaseJsService(IJSRuntime JsRuntime, IUriHelper uriHelper, GlobalStateChange globalStateHasChanged, FirebaseAdminService firebaseAdminService)
        {
            _jsRuntime = JsRuntime;
            _uriHelper = uriHelper;
            _firebaseAdminService = firebaseAdminService;
            _globalStateHasChanged = globalStateHasChanged;
        }

        private async Task<bool> CreateUserAsync(BaseUser newUser)
        {
            try
            {
                var result = await _jsRuntime.InvokeAsync<BaseUser>("firebaseInterop.signUpNewUser"
                                                , newUser.Email
                                                , newUser.Password);
                if (result.Token == null)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                return false;
            }
        }
        public async Task<bool> SignUpUserAsync(BaseUser newUser)
        {

            return await CreateUserAsync(newUser);
        }

        private async Task<bool> SignOut()
        {
            _user = new BaseUser();
            _uriHelper.NavigateTo("/login");
            _globalStateHasChanged.InvokeStateChange();
            return true;
            //try
            //{
            //    bool result = await _jsRuntime.InvokeAsync<bool>("firebaseInterop.logOut"
            //                                   , DotNetObjectRef.Create(this));
            //    return true;
            //}
            //catch (Exception e)
            //{

            //    Console.WriteLine(e);
            //    return false;
            //}
        }
        public async Task<bool> LogOut()
        {
            return await SignOut();

        }

        private async Task<bool> SigninUser(BaseUser user)
        {
            try
            {
                    var result = await _jsRuntime.InvokeAsync<BaseUser>("firebaseInterop.signinUser"
                                               , DotNetObjectRef.Create(this)
                                               , user.Email
                                               , user.Password);
                if (result.DisplayName != string.Empty)
                {
                    _user = result;
                }
                else
                {
                    return false;
                }
                return true;
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                return false;
            }
        }
        public async Task<bool> SignIn(BaseUser User)
        {
            return await SigninUser(User);
        }

        private async Task<bool> SignInUserWithGooglePopup()
        {
            try
            {
                var result = await _jsRuntime.InvokeAsync<BaseUser>("firebaseInterop.signInGooglePopup"
                                              , DotNetObjectRef.Create(this));
                return true;
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                return false;
            }
        }
        public async Task<bool> SignInWithGooglePopup()
        {
           return await SignInUserWithGooglePopup();
        }

        public Task<bool> UpdateUser()
        {
            throw new NotImplementedException();
        }

        private async Task<BaseUser> GetCurrentUserFromFirebase()
        {
            return _user;
            //try
            //{
            //    var result = await _jsRuntime.InvokeAsync<BaseUser>("firebaseInterop.getCurrentuser");
            //    if (result.Token == null)
            //    {
            //        _user = result;
            //        return _user;
            //    }
            //    else
            //    {
            //        return result;
            //    }
            //}
            //catch (Exception e)
            //{
            //    Console.WriteLine(e);
            //    return new BaseUser();
            //}
        }
        public async Task<BaseUser> GetCurrentUser()
        {
            return await GetCurrentUserFromFirebase();
        }

        [JSInvokable]
        public async Task UserSignInResult(BaseUser user, string token)
        {
            Console.WriteLine(user);
            try
            {
           
                if (user.DisplayName != string.Empty)
                {
                    this._user = user;
                    this._user.Token = token;

                    _uriHelper.NavigateTo("/");
                }
                else
                {
                    _uriHelper.NavigateTo("/login");
                }
                this._globalStateHasChanged.InvokeStateChange();
            }
            catch (Exception e)
            {
                throw;
            }
           
        }


        [JSInvokable]
        public async Task UserLogOutResult(bool result)
        {
            
            try
            {

                if (!result)
                {
                    _uriHelper.NavigateTo("/");
                }
                else
                {
                    _uriHelper.NavigateTo("/login");
                }
                this._globalStateHasChanged.InvokeStateChange();
            }
            catch (Exception e)
            {
                throw;
            }

        }

        private async Task<bool> IsUserSignIn()
        {
            try
            {
                var result = await _firebaseAdminService.VerifyIdToken(_user.Token);
                if (result)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
                return false;
            }
        
        }
        public async Task<bool> IsSignIn()
        {
          return await IsUserSignIn();
        }
    }
}
