﻿using SpeakMentoriship.Core.DTO.SpeakmentorshipDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SpeakMentoriship.Core.Services
{
    public interface IFirebaseJsService
    {
        Task<bool> SignUpUserAsync(BaseUser newUser);
        Task<bool> UpdateUser();
        Task<bool> SignIn(BaseUser user);
        Task<bool> LogOut();
        Task<bool> SignInWithGooglePopup();
        Task<BaseUser> GetCurrentUser();
        Task<bool> IsSignIn();
        //JSInvokable
         Task UserSignInResult(BaseUser user, string token);
        Task UserLogOutResult(bool result);
    }
}
