﻿using SpeakMentoriship.Core.DTO.SpeakmentorshipDTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace SpeakMentoriship.Core.Services
{
    public interface IFirebaseAdminService
    {

        Task<bool> CreateUser(BaseUser newUser);
        Task<bool> UpdateUser(string userId, BaseUser updatedUser);
        Task<bool> DeleteUser();
        Task<bool> LogInUser();
        Task<bool> LogOutUser();
        Task<string> CreateCustomToken(string uid);
        Task<bool> VerifyIdToken(string token);
    }
}
