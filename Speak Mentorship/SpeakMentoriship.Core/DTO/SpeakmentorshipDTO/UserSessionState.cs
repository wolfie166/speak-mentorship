﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakMentoriship.Core.DTO.SpeakmentorshipDTO
{
    public class UserSessionState
    {
        public bool IsSignIn { get; set; } = false;
        public string Email { get; set; } 
        public string RefreshToken { get; set; }
        public string DisplayName { get; set; }
    }
}
