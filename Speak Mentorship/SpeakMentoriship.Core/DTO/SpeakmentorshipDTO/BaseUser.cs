﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SpeakMentoriship.Core.DTO.SpeakmentorshipDTO
{
    public class BaseUser
    {
        public string Email { get; set; } = string.Empty;
        public bool EmailVerified { get; set; } = false;
        public string PhoneNumber { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
        public string DisplayName { get; set; } = string.Empty;
        public string PhotoUrl { get; set; } = string.Empty;
        public bool Disabled { get; set; } = false;
        public string Token { get; set; } = string.Empty;
    }
}
