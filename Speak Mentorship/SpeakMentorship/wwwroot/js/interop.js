﻿window.exampleJsFunctions = {
    sweetAlert: function (title, text, type, showCancelButton, confirmButtonText, cancelButtonText) {
        $(document).ready(function () {
            Swal.fire({
                title: title,
                text: text,
                type: type,
                showCancelButton: showCancelButton,
                confirmButtonText: confirmButtonText,
                cancelButtonText: cancelButtonText
            }).then((result) => {
                return result.value;
            });
        });
    },
    starRatingInit: function (elementId, min, max, step) {
        // initialize with defaults
        //var element = "#" + elementId;
        $(elementId).rating();

        // with plugin options
        $(element).rating({ min: min, max: max, step: step, size: 'lg' });
    }
    
};
