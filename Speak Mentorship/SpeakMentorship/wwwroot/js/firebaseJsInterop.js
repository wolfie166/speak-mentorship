﻿window.firebaseInterop = {
    initConfig: function () {
        //Your web app's Firebase configuration
        var firebaseConfig = {
            apiKey: "AIzaSyDsIHDc3GnXEgsAoEVpUUdcUv5fI2DPGIg",
            authDomain: "speakmentorship.firebaseapp.com",
            databaseURL: "https://speakmentorship.firebaseio.com",
            projectId: "speakmentorship",
            storageBucket: "",
            messagingSenderId: "697999343503",
            appId: "1:697999343503:web:6d544d9cc366aea5"
        };


        // Initialize Firebase
        firebase.initializeApp(firebaseConfig);
    },
    uiInit: function (successRedirect, badRedirect) {

        try {
            var ui = firebaseui.auth.AuthUI.getInstance() || new firebaseui.auth.AuthUI(firebase.auth());
        } catch (e) {
            //  console.log(e);
        }

        var uiConfig = {
            callbacks: {
                signInSuccessWithAuthResult: function (authResult, redirectUrl) {
                    // User successfully signed in.
                    // Return type determines whether we continue the redirect automatically
                    // or whether we leave that to developer to handle.
                    console.log("This is the firebase auth result: ");
                    console.log(authResult);
                    if (authResult.additionalUserInfo.providerId === "password") {
                        //this is a local platform user
                    } else if (authResult.additionalUserInfo.providerId === "google.com") {
                        //this is a google platform user
                    }
                    return authResult.additionalUserInfo;
                },
                uiShown: function () {
                    // The widget is rendered.
                    // Hide the loader.
                    document.getElementById('loader').style.display = 'none';
                }
            },
            // Will use popup for IDP Providers sign-in flow instead of the default, redirect.
            signInFlow: 'popup',
            signInSuccessUrl: successRedirect,
            signInOptions: [
                // Leave the lines as is for the providers you want to offer your users.
                firebase.auth.EmailAuthProvider.PROVIDER_ID,
                firebase.auth.GoogleAuthProvider.PROVIDER_ID

            ],
            // Terms of service url.
            tosUrl: '/',
            // Privacy policy url.
            privacyPolicyUrl: '<your-privacy-policy-url>'
        };

        // The start method will wait until the DOM is loaded.
        try {
            ui.start('#firebaseui-auth-container', uiConfig);
        } catch (e) {
            //console.log(e);
        }
    },
    signUpNewUser: function (email, password) {
        var promise = firebase.auth().createUserWithEmailAndPassword(email, password).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // ...
        });

        promise.catch(e => console.log(e.message));
    },
    signinUser: function (dotnetHelper, email, password) {
        firebase.auth().signOut().then(function (result) {
            // Sign-out successful.
            var promise = firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
                // Handle Errors here.
                var errorCode = error.code;
                var errorMessage = error.message;
                // ...
            }).then(function (result) {


                var user = firebase.auth().currentUser;
                console.log("Current User: " + user);
                var uid = user.uid;
                firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function (idToken) {
                    // Send token to your backend via HTTPS
                    console.log(idToken);

                    try {
                        return dotnetHelper.invokeMethodAsync('UserSignInResult', user, idToken);
                    } catch (e) {
                        console.log(e);
                    }
                }).catch(function (error) {
                    // Handle error
                });
          
            });

        }).catch(function (error) {
            // An error happened.
        });
       

    },
    isUserignIn: function (dotnetHelper) {
        var user = firebase.auth().currentUser;
        if (user) {
            return dotnetHelper.invokeMethodAsync('UserSignInResult', user)
                .then(r => console.log(r));
        } else {
            return false;
        }
    },
    getCurrentuser: function () {
        var user = firebase.auth().currentUser;
        return user;
    },
    sigInWithRedirect: function () {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithRedirect(provider);

        firebase.auth().getRedirectResult().then(function (result) {
            if (result.credential) {
                // This gives you a Google Access Token. You can use it to access the Google API.
                var token = result.credential.accessToken;
                // ...
            }
            // The signed-in user info.
            var user = result.user;
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
        });
    },
    signInGooglePopup: function (dotnetHelper, providerId) {
        firebase.auth().signOut().then(function (result) {
            // Sign-out successful.

        }).catch(function (error) {
            // An error happened.
        });
        var provider = new firebase.auth.GoogleAuthProvider();
        var promise = firebase.auth().signInWithPopup(provider).then(function (result) {
            var user = firebase.auth().currentUser;
            firebase.auth().currentUser.getIdToken(/* forceRefresh */ true).then(function (idToken) {
                // Send token to your backend via HTTPS
                console.log(idToken);

                try {
                    return dotnetHelper.invokeMethodAsync('UserSignInResult', user, idToken);
                } catch (e) {
                    console.log(e);
                }
            }).catch(function (error) {
                // Handle error
            });
        }).catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // The email of the user's account used.
            var email = error.email;
            // The firebase.auth.AuthCredential type that was used.
            var credential = error.credential;
            // ...
            return;
        });

        promise
            .then(result => console.log(result.user.providerData))
            .catch(e => console.log(e.message));

     
    },
    sendVerificationEmail: function () {
        var user = firebase.auth().currentUser;

        user.sendEmailVerification().then(function () {
            return true;
        }).catch(function (error) {
            // An error happened.
            return false;
        });
    },
    updateUserPassword: function (password) {
        var user = firebase.auth().currentUser;
        var newPassword = password;

        user.updatePassword(newPassword).then(function () {
            // Update successful.
            return true;
        }).catch(function (error) {
            // An error happened.
            return false;
        });
    },
    sendPasswordResetEmail: function (email) {
        var auth = firebase.auth();

        auth.sendPasswordResetEmail(email).then(function () {
            // Email sent.
            return true;
        }).catch(function (error) {
            // An error happened.
            return false;
        });
    },
    deleteUser: function () {
        var user = firebase.auth().currentUser;

        user.delete().then(function () {
            // User deleted.
            return true;
        }).catch(function (error) {
            // An error happened.
            return false;
        });
    },
    reauthenticateUser: function (email, password) {
        //var promise = firebase.auth().signInWithEmailAndPassword(email, password).catch(function (error) {
        //    // Handle Errors here.
        //    var errorCode = error.code;
        //    var errorMessage = error.message;
        //    // ...
        //    return false;
        //});

        //promise
        //    .then(result => console.log(result.user.providerData))
        //    .catch(e => console.log(e.message));
        //var user = firebase.auth().currentUser;
        //// Prompt the user to re-provide their sign-in credentials

        //user.reauthenticateWithCredential(credential).then(function () {
        //    // User re-authenticated.
        //}).catch(function (error) {
        //    // An error happened.
        //});
    },
    getIdToken: function () {
        firebase.auth().currentUser.getIdToken()
            .then((idToken) => {
                // idToken can be passed back to server.
                return token;
            })
            .catch((error) => {
                // Error occurred.
                return false;
            });
    },
    logOut: function (dotnetHelper) {
        firebase.auth().signOut().then(function () {
            // Sign-out successful.
            return dotnetHelper.invokeMethodAsync('UserLogOutResult', user, user.refreshToken);
        }).catch(function (error) {
            return false;
            // An error happened.
        });
    }

};
