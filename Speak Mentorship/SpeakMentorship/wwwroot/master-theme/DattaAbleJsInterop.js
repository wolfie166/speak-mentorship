﻿window.dattaAble = {

    smartWizardInit: function () {
        $(document).ready(function () {
            setTimeout(function () {
                $('#smartwizard').smartWizard({
                    theme: 'dots',
                    transitionEffect: 'fade',
                    autoAdjustHeight: false,
                    useURLhash: false,
                    showStepURLhash: false
                });
            }, 700);
            $("#theme_selector").on("change", function () {
                $('#smartwizard').smartWizard("theme", $(this).val());
                return true;
            });
            $('#smartwizard .sw-toolbar').appendTo($('#smartwizard .sw-container'));
            $("#theme_selector").change();
        });


    },
    cssStarRating: function () {
        function ratingEnable() {
            $('.rating-star').barrating({
                theme: 'css-stars',
                showSelectedRating: false
            });
        }
            
        ratingEnable();
       
    },
    datePickerInit: function () {
        
        try {
            $('#date').bootstrapMaterialDatePicker({
                weekStart: 0,
                time: false
            });
            //$(window).ready(function () {
            //    $('#date').datepicker({
            //        autoclose: true
            //    });
            //});
            return true;
        } catch (e) {
            console.log(e);
            return false;
        }
       
    },
    notFoundComponentInit: function () {
        var scene = document.getElementById('scene');
        var parallax = new Parallax(scene);
    },
    selectInputInit: function () {
        $(document).ready(function () {
           

            try {
                // [ diacritics select ] start
                $(".js-example-diacritics").select2();

              

            } catch (e) {
                console.log(e);
            }
           
        });
    }

};
