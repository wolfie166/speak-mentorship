﻿const path = require("path");

module.exports = {
    mode: 'development',
    entry: {
        './wwwroot/master-theme/dattaAbleJsInterop.js',
        './wwwroot/master-theme/assets/js/vendor-all.min.js',
        './wwwroot/master-theme/assets/plugins/bootstrap/js/bootstrap.min.js',
        './wwwroot/master-theme/assets/js/pcoded.min.js',
        './wwwroot/master-theme/assets/plugins/prism/js/prism.min.js',
        './wwwroot/master-theme/assets/plugins/ratting/js/jquery.barrating.min.js',
        './wwwroot/master-theme/assets/plugins/smart-wizard/js/jquery.smartWizard.min.js',
        './wwwroot/master-theme/assets/js/pages/wizard-custom.js',
        './wwwroot/master-theme/assets/plugins/material-datetimepicker/js/bootstrap-material-datetimepicker.js'
    },
   
    output: {
        path: path.resolve(__dirname, '../wwwroot/js'),
        filename: "bundle.js"
    }
};